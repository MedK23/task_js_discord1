class User {
    constructor(name,email){
        this.id = generateId()  
        this.name = name
        this.email = email
    }

    toString(){
        return 'ID : '+ this.id + ' ' +'Name : '+ this.name + ' '+'Email : '+ this.email
    }
}

async function getEmailDomain(){
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const data = await response.json();
        let domainArray = []
        data.map(user => {
            const domain = user.email.split('@'); 
            domainArray.push(domain[1])
        })
        return console.log(domainArray);
    }

   
       
    function generateId(){
        const randomNum= Math.floor(Math.random() * 1000);
        return randomNum 
      }



getEmailDomain()
//
const users = []
const u1 = new User("ali", "ali@gmail.com")
const u2 = new User("med","med@gmail.com")
const u3 = new User("Jack","jack@gmail.com")

users.push(u1)
users.push(u2)
users.push(u3)


console.log("**********************************")

const filteredUsers = users.filter(user => user.name.startsWith("J"));
console.log(filteredUsers);


const userNames = users.map(user => user.name);
console.log(userNames);


users.forEach(user => console.log(user));
